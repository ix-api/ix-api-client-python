
# Python Client for IX-API

This library provides a wrapper around `requests` with
convenience functions to interact with the ix-api.

## Supported Versions

Right now, only v2 is supported.

## Usage

Make sure all requirements are installed e.g. in a virtual environment

    python3 -m venv venv/
    . venv/bin/activate
    pip3 install -r requirements.txt

Then you can start the shell:

    ./bin/ix-api-shell -H https://ixapi.myixp.example/api/v2 -k myapkey -s 1xAP1s3cr3t

or, if your implementation provides a [`test-suite-config.json`](https://gitlab.com/ix-api/ix-api-schema/-/wikis/API-Testing)

    ./bin/ix-api-shell -C https://ixapi.myix.example/static/test-suite-config-v2.json

## Local Scripts

You can prepare scripts and helper functions by importing
python modules. For your convenience, if a `./local` directory
is present, it is ignored in git and added to the pythonpath.

You can directly import e.g. ./local/demo1.py in your shell:

    import demo1


## Containers

You can use the `Dockerfile` to build and run the ix-api-client:

    docker build . -t ix-api-client:latest

And then use it like this:

    docker run --rm -it ix-api-client:latest -H https://ixapi.myixp.example/api/v2 -k myapkey -s 1xAP1s3cr3t

With local scripts:

    docker run --rm -it -v /my/script/path:/code/local ix-api-client:latest ...
    

