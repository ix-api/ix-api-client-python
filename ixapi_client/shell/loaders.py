

import os
import glob
import importlib


def load_package_path(path):
    """Load all modules in path"""
    return dict(_load_module_filename(s)
                for s in glob.glob(os.path.join(path, "*.py"))
                if "__init__" not in s)


def _load_module_filename(path):
    """Load a script from a given path"""
    # Derive module name
    fqmn = path.replace(".py", "") \
        .replace(".", "") \
        .replace("/", ".")

    tokens = fqmn.split(".")
    module_name = tokens[-1]

    # Load the module
    module = importlib.import_module(fqmn)

    return (module_name, module)

