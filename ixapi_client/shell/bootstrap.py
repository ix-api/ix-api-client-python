
"""
Bootstrap the ix-api shell
"""

# For convenience:
import sys
import json
import textwrap
from pprint import pprint
from importlib import reload

from requests import exceptions as requests_exceptions

from ixapi_client.v2 import (
    client,
    exceptions as v2_exc,
)
from ixapi_client.shell import loaders
from ixapi_client.shell.text import print_f


def init(args):
    """Initialize the api shell"""
    return init_v2(args)


def init_v2(args):
    """Initialize shell for use with v1 api"""
    def dial():
        """Create new client session"""
        return client.dial(args.api_base_url, args.api_key, args.api_secret)

    try:
        session = dial()
    except v2_exc.AuthenticationError as exc:
        print_f("<red>The session could not be authenticated.</red>")
        print_f("Please check:")
        print_f(" - The API host; it should contain the full path")
        print_f("   with schema and version praefix.")
        print_f("   e.g. <i>https://my.ix-api.example/api/v2</i>")
        print_f(" - The API-Key and Secret you are using are correct.")
        print()
        print_auth_exc(exc)
        sys.exit(-1)

    # Resources
    resources = loaders.load_package_path("ixapi_client/v2/resources")
    print_f("<cyan><b>Available Resources:</b></cyan>")
    for name, _ in resources.items():
        print(" - {}".format(name))
    print("")

    return {
        "session": session,
        "s": session,
        "dial": dial,
        "reload": reload,
        "args": args.__dict__,
        **resources,
        **args.__dict__,
    }


def print_auth_exc(exc):
    """Print the authentication error if decodable"""
    try:
        err = json.loads(str(exc))
    except json.JSONDecodeError:
        err = str(exc)

    if isinstance(err, dict):
        # We have a structured error, so we pretty print
        print_f("<b>Error details:</b>")
        pprint(err)
    else:
        print_f("<b>Received unexpected response from server:</b>")
        print(textwrap.shorten(err, width=400))
    print()
