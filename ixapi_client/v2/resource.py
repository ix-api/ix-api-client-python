"""
Resource decorators
"""

import functools
from urllib.parse import urlparse

# from ixapi_client.openapi import validation
# from ixapi_client.spec import openapi
# from ixapi_client.spec.exceptions import ValidationError

# Initialize validation
# spec = openapi.load_spec("schema/ix-api-schema-v1.json")
#
# NOTE: I'm not satisfied with the way the
#       decorator is used here as it shadows the
# A better (more declarative) way would be separating
# the operation from the execution.
#
# So, instead of resource.get_network_services(session, {...}),
# I now think this should be done more along the lines of
# session(resource.get_network_Services(...)).
#

def api_get(fn):
    @functools.wraps(fn)
    def decorator(session, *args, filters=None, **kwargs):
        """Api GET request"""
        url = fn(*args, **kwargs)
        return session.get(url, filters)
    return decorator


def api_post(fn):
    @functools.wraps(fn)
    def decorator(session, *args, **kwargs):
        """Api POST request"""
        url, request = fn(*args, **kwargs)
        return session.post(url, request)
    return decorator


def api_put(fn):
    @functools.wraps(fn)
    def decorator(session, *args, **kwargs):
        """Api PUT request"""
        url, request = fn(*args, **kwargs)
        return session.put(url, request)
    return decorator


def api_patch(fn):
    @functools.wraps(fn)
    def decorator(session, *args, **kwargs):
        """Api PATCH request"""
        url, request = fn(*args, **kwargs)
        return session.patch(url, request)
    return decorator


def api_delete(fn):
    @functools.wraps(fn)
    def decorator(session, *args, filters=None, **kwargs):
        """Api DELETE request"""
        url = fn(*args, **kwargs)
        return session.delete(url, filters)
    return decorator


def api_update(fn):
    """
    Add optional partial keyword argument,
    use either api_put or patch.
    """
    @functools.wraps(fn)
    def decorator(session, *args, partial=True, **kwargs):
        """Api PATCH/PUT request"""
        url, request = fn(*args, **kwargs)
        if partial:
            return session.patch(url, request)
        return session.put(url, request)

    return decorator


def status_check(fn):
    """
    Optional error check:
    Enables raise_on_error (Default: False) kwarg
    """
    @functools.wraps(fn)
    def decorator(*args, raise_on_error=False, **kwargs):
        """Decorate calling function"""
        response, payload = fn(*args, **kwargs)
        if raise_on_error:
            response.raise_for_status()
        return (response, payload)

    return decorator


def _path_pattern_from_url(url):
    """Make path pattern from url"""
    parsed_url = urlparse(url)
    path = parsed_url.path
    path = path.replace("/v1", "")
    tokens = path.split("/")[1:]
    if len(tokens) == 1:
        return "/" + tokens[0]
    elif len(tokens) == 2:
        return "/" + tokens[0] + "/{id}"

    raise ValueError("Unacceptable url: {}".format(url))


def decode_with(decoder, *decoder_args, **decoder_kwargs):
    """Decode response with decoder function"""
    def wrapper(fn):
        @functools.wraps(fn)
        def decorator(*args, **kwargs):
            """Run function and call decoder with result"""
            response, payload = fn(*args, **kwargs)
            return decoder(
                response, payload, *decoder_args, **decoder_kwargs)
        return decorator
    return wrapper
