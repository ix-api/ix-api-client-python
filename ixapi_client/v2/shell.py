"""
Shell v2 Entrypoint
"""
from ixapi_client.shell.cli import __main__

if __name__ == "__main__":
    __main__()
