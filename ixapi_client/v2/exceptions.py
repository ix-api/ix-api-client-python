
"""
API Exceptions
"""

class ClientError(Exception):
    """Client could not handle response errors"""

class ServerError(Exception):
    """Server could not handle request"""

class ApiError(Exception):
    """Exception base class"""

class AuthenticationError(ServerError, ApiError):
    """Server returned an authentication error"""

class MalformedResponseBody(ClientError, ApiError):
    """Server responded with malformed json"""

class EnvelopeError(ClientError, ApiError):
    """The response envelope is broken"""

class InvalidResponseBody(ClientError, ApiError):
    """Server responded body not conforming to the OpenAPI spec"""

class TokenError(ClientError, ApiError):
    """JWT issued by the server was broken"""

class InvalidToken(TokenError):
    """JWT issued by the server was broken"""
