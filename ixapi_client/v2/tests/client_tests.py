
"""
Test the JEA test suite api client
"""

import pytest
import jwt
import mock
from mock import patch
from datetime import datetime, timedelta

from ixapi_client.v1 import client
from ixapi_client.v1.exceptions import AuthenticationError


def _make_token():
    """Make some test token"""
    now = datetime.utcnow()
    payload = {
        "sub": 23,
        "iat": now,
        "exp": now + timedelta(minutes=5),
    }


@patch("requests.post")
@patch("ixapi_client.v1.tokens.ApiToken.from_jwt")
def test_session_refresh(from_jwt, requests_post):
    """Test session refreshing"""
    mock_client = mock.MagicMock()
    access_token = mock.MagicMock()
    refresh_token = "TOKEN"

    # Mocked response
    from_jwt.side_effect = ["access_token", "refresh_token"]

    session = client.Client("host", access_token, refresh_token)

    refreshed = session.refresh()
    assert id(session) != id(refreshed), \
        "A new session object should be created"

    # Test self refresh
    from_jwt.side_effect = ["access_token", "refresh_token"]
    refreshed = session.refresh(update=True)
    assert id(session) == id(refreshed), \
        "When using update the same session should be reused"


@patch("requests.post")
@patch("ixapi_client.v1.tokens.ApiToken.from_jwt")
def test_session_refresh(from_jwt, requests_post):
    # Mocked response
    from_jwt.side_effect = ["access_token", "refresh_token"]
    c = client.dial("host", "api_key", "api_secret")

    assert c.access_token == "access_token"
    assert c.refresh_token == "refresh_token"


