
from ixapi_client.v2.resource import (
    status_check,
    api_get,
)


@status_check
@api_get
def health_retrieve():
    """Get current api health status"""
    return "/health"
