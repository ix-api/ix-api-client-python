
"""
Catalog Resources
"""

from ixapi_client.v2.resource import (
    status_check,
    api_get,
)


## Devices

@status_check
@api_get
def devices_list():
    """Get all devices"""
    return "/devices"


@status_check
@api_get
def devices_retrieve(pk):
    """Get a device by id"""
    return f"/devices/{pk}"


## Pops

@status_check
@api_get
def pops_list():
    """Get all pops"""
    return "/pops"


@status_check
@api_get
def pops_retrieve(pk):
    """Get a pop by id"""
    return f"/pops/{pk}"


## Facilities

@status_check
@api_get
def facilities_list():
    """Get all facilities"""
    return "/facilities"


@status_check
@api_get
def facilities_retrieve(pk):
    """Get a facility by id"""
    return f"/facilities/{pk}"


## Products

@status_check
@api_get
def product_offerings_list():
    """Get all offerings"""
    return "/product-offerings"


@status_check
@api_get
def product_offerings_retrieve(pk):
    """Get a product by id"""
    return f"/product-offerings/{pk}"


@status_check
@api_get
def metro_area_networks_list():
    """List all metro area networks"""
    return "/metro-area-networks"


@status_check
@api_get
def metro_area_networks_retrieve(pk):
    """Get a MAN by ID"""
    return f"/metro-area-networks/{pk}"


@status_check
@api_get
def metro_areas_list():
    """List all metro areas"""
    return "/metro-areas"


@status_check
@api_get
def metro_areas_retrieve(pk):
    """Get a metro area by id"""
    return f"/metro-areas/{pk}"
