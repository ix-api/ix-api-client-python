
"""
Config Resources
"""

from ixapi_client.v2.resource import (
    status_check,
    api_get,
    api_post,
    api_update,
    api_delete,
)


## Ports 

@status_check
@api_get
def ports_list():
    """List ports"""
    return "/ports"


@status_check
@api_get
def ports_retrieve(pk):
    """Get demarc"""
    return f"/ports/{pk}"


## Connections

@status_check
@api_get
def connections_list():
    """List connections"""
    return "/connections"


@status_check
@api_get
def connections_retrieve(pk):
    """Get a connection"""
    return f"/connections/{pk}"


## Network Service Configs

@status_check
@api_get
def network_service_configs_list():
    """List network service config"""
    return "/network-service-configs"


@status_check
@api_get
def network_service_configs_retrieve(pk):
    """Get a network service config"""
    return f"/network-service-configs/{pk}"


@status_check
@api_post
def network_service_configs_create(request):
    """Create a network service configuration"""
    return "/network-service-configs", request


@status_check
@api_update
def network_service_configs_update(pk, update):
    """Update a network service config"""
    return f"/network-service-configs/{pk}", update


@status_check
@api_delete
def network_service_configs_destroy(pk):
    """Remove a network service config"""
    return f"/network-service-configs/{pk}"


## Network Feature Configs

@status_check
@api_get
def network_feature_configs_list():
    """List network feature config"""
    return "/network-feature-configs"


@status_check
@api_get
def network_feature_configs_retrieve(pk):
    """Get a network feature config"""
    return f"/network-feature-configs/{pk}"


@status_check
@api_post
def network_feature_configs_create(request):
    """Create a network feature configuration"""
    return "/network-feature-configs", request


@status_check
@api_update
def network_feature_configs_update(pk, update):
    """Update a network feature config"""
    return f"/network-feature-configs/{pk}", update


@status_check
@api_delete
def network_feature_configs_destroy(pk):
    """Remove a network feature config"""
    return f"/network-feature-configs/{pk}"

