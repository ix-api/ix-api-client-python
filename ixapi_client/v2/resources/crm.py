
"""
CRM Resources
"""

from ixapi_client.v2.resource import (
    status_check,
    api_get,
    api_post,
    api_patch,
    api_update,
    api_delete,
)


@status_check
@api_get
def accounts_list():
    """Get all customer accounts"""
    return "/accounts"


@status_check
@api_get
def accounts_retrieve(pk):
    """Get an account by pk"""
    return f"/accounts/{pk}"


@status_check
@api_post
def accounts_create(request):
    """Create a customer"""
    return "/accounts", request


@status_check
@api_update
def accounts_update(pk, request):
    """Update an account"""
    return f"/accounts/{pk}", request


@status_check
@api_delete
def accounts_destroy(pk):
    """Destroy an account"""
    return f"/accounts/{pk}"

@status_check
@api_get
def contacts_list():
    """get all accounts"""
    return "/contacts"


@status_check
@api_get
def contacts_retrieve(pk):
    """Get a contact"""
    return f"/contacts/{pk}"


@status_check
@api_post
def contacts_create(request):
    """Create a contact"""
    return "/contacts", request


@status_check
@api_update
def contacts_update(pk, request):
    """Update a contact"""
    return f"/contacts/{pk}", request


@status_check
@api_get
def roles_list():
    """List all contact roles"""
    return f"/roles"


@status_check
@api_get
def roles_retrieve(pk):
    """Get a role by key"""
    return f"/roles/{pk}"


@status_check
@api_get
def role_assignments_list():
    """Get role assignments"""
    return "/role-assignments"


@status_check
@api_get
def role_assignments_retrieve(pk):
    """Get role asssignment by ID"""
    return f"/role-assignments/{pk}"


@status_check
@api_post
def role_assignments_create(request):
    """Create a role assignment"""
    return f"/role-assignments", request


@status_check
@api_delete
def role_assignments_destroy(pk):
    """Remove a role assignment"""
    return f"/role-assignments/{pk}"
