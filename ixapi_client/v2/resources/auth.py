
"""
Authorization Helpers
"""

from ixapi_client.v2 import exceptions
from ixapi_client.v2.resource import api_post, decode_with
from ixapi_client.v2.tokens import ApiToken


def authorize_as_last_subcustomer(client, session):
    """
    As a root customer we get the customers list and
    choose the last created customer from it and
    open a new session with this user.
    """
    _, customers = session.get("/customers?state=production")
    if not customers:
        raise Exception("No customers present.")

    customers = sorted(customers, key=lambda c: c["id"])
    customer = customers[-1] # Pick latest

    print("")
    print("Authorizing as customer: {} ({})".format(
        customer["name"],
        customer["id"]))

    return client.dial(customer["id"])


def _decode_token_response(response, payload):
    """
    Decode a token response into an access and refresh token.
    """
    if not response.ok:
        raise exceptions.AuthenticationError(response.text)

    try:
        access_token_jwt = payload["access_token"]
        refresh_token_jwt = payload["refresh_token"]
    except KeyError as err:
        raise exceptions.AuthenticationError(
            "Missing token: {}".format(err))

    access_token = ApiToken.from_jwt(access_token_jwt)
    refresh_token = ApiToken.from_jwt(refresh_token_jwt)

    return access_token, refresh_token


@decode_with(_decode_token_response)
@api_post
def auth_token_create(request):
    """Create an auth toke"""
    return "/auth/token", request


@decode_with(_decode_token_response)
@api_post
def auth_token_refresh(request):
    """Refresh the auth token"""
    return "/auth/refresh", request
