
"""
Access Resources
"""

from ixapi_client.v2.resource import (
    status_check,
    api_get,
    api_post,
    api_delete,
    api_update,
)


## Network Services

@status_check
@api_get
def network_services_list():
    """List all network services"""
    return "/network-services"


@status_check
@api_get
def network_services_retrieve(pk):
    """Get a network service by id"""
    return f"/network-services/{pk}"


@status_check
@api_post
def network_services_create(request):
    """Create network service"""
    return "/network-services", request


@status_check
@api_update
def network_services_update(pk, request):
    """Update a network service by id"""
    return f"/network-services/{pk}", request


@status_check
@api_delete
def network_services_destroy(pk):
    """Delete a network service by ID"""
    return f"/network-services/{pk}"

# Change Requests

@status_check
@api_get
def network_service_change_request_retrieve(pk):
    """Get the pending change request for a network service"""
    return f"/network-services/{pk}/change-request"


@status_check
@api_post
def network_service_change_request_create(pk, request):
    """Get the pending change request for a network service"""
    return f"/network-services/{pk}/change-request", request


@status_check
@api_delete
def network_service_change_request_destroy(pk):
    """Cancel a pending change request"""
    return f"/network-services/{pk}/change-request"


## Network Features

@status_check
@api_get
def network_features_list():
    """List all network features"""
    return "/network-features"


@status_check
@api_get
def network_features_retrieve(pk):
    """Get a network service by id"""
    return f"/network-features/{pk}"


@status_check
@api_get
def member_joining_rules_list():
    """Get member joining rules"""
    return "/member-joining-rules"


@status_check
@api_get
def member_joining_rules_retrieve(pk):
    """Get member joining rules"""
    return f"/member-joining-rules/{pk}"


@status_check
@api_post
def member_joining_rules_create(request):
    """Get member joining rules"""
    return "/member-joining-rules", request


@status_check
@api_update
def member_joining_rules_update(pk):
    """update member joining rules"""
    return f"/member-joining-rules/{pk}"


@status_check
@api_delete
def member_joining_rules_destroy(pk):
    """update member joining rules"""
    return f"/member-joining-rules/{pk}"
