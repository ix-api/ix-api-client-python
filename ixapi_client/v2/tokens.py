
"""
JEA Access and Refresh Tokens
"""

from datetime import datetime

import jwt

from ixapi_client.v2 import exceptions


class ApiToken:

    def __init__(
            self,
            issued_at,
            expires_at,
            token,
        ):
        """
        Initialize token
        """
        self.issued_at = issued_at
        self.expires_at = expires_at
        self.token = token # The original signed token data

    def __str__(self):
        """Return token data"""
        return self.token

    def __repr__(self):
        """Make representation"""
        return "<ApiToken issued_at={} ttl={}>".format(self.issued_at, self.ttl)

    @classmethod
    def from_jwt(cls, token):
        """
        Decode JWT and set api token fields
        """
        try:
            payload = jwt.decode(token, verify=False)
        except Exception as exc:
            raise exceptions.TokenError(exc)

        try:
            issued_at = datetime.utcfromtimestamp(payload["iat"])
            expires_at = datetime.utcfromtimestamp(payload["exp"])
            return cls(issued_at,
                       expires_at,
                       token)
        except Exception as exc:
            raise exceptions.InvalidToken(exc)

    @property
    def ttl(self):
        """Calculate tokens ttl in seconds"""
        now = datetime.utcnow()
        ttl_seconds = (self.expires_at - now).total_seconds()

        return max(0, ttl_seconds)

