"""
The ixapi_client provides a client for the v2 ix-api.
"""

__title__ = 'ix-api-client'
__author__ = 'Annika Hannig'
__license__ = 'Apache 2.0'
__copyright__ = 'Copyright 2018-2020, The IX-API Working Group'
__version__ = '2.0.0'

