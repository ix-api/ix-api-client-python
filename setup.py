#!/usr/bin/env python3

"""
IX-API: Client
==============

This package contains the ix-api client library
for python.
"""

from setuptools import setup, find_packages


from ixapi_client import (
    __title__,
    __license__,
    __author__,
    __version__,
)

# Load requirements
with open("requirements.txt") as f:
    requirements = f.read().splitlines()

# Load description
with open("README.md") as f:
    long_description = f.read()


setup(name=__title__,
      version=__version__,
      packages=find_packages(),
      install_requires=requirements,
      license=__license__,
      long_description=long_description)
