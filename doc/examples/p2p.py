
from ixapi_client.v2.resources import catalog, service, config, crm


def get_offerings(s, args):
    """Get offerings"""
    return catalog.product_offerings_list(s, filters={
        "type": "p2p_vc",
    })


def create_ns(s, args, offering):
    """Create network service"""
    account = args["account"]
    subaccount1 = args["subaccounts"][0]
    subaccount2 = args["subaccounts"][1]
    return service.network_services_create(s, {
        "type": "p2p_vc",
        "billing_account": account["id"],
        "managing_account": account["id"],
        "consuming_account": subaccount1["id"],
        "product_offering": offering,
        "joining_member_account": subaccount2["id"],
    })


def create_p2p_vc_nsc(s, args, subaccount, ns_id):
    account = args["account"]
    ret, nsc = config.network_service_configs_create(s, {
        "type": "p2p_vc",
        "network_service": ns_id,
        "role_assignments": [
            subaccount["implementation_role_assignment_id"],
            subaccount["noc_role_assignment_id"],
        ],
        "connection": subaccount["connection_id"],
        "billing_account": account["id"],
        "managing_account": account["id"],
        "consuming_account": subaccount["id"],
        "vlan_config": {
            "vlan_type": "dot1q",
            "vlan": 45,
            "vlan_ethertype": "0x8100",
        },
    })

    return ret, nsc
