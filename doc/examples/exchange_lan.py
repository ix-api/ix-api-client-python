"""
Demo tools
"""


from ixapi_client.v2.resources import config, service


def create_nsc(s, args, ns_id):
    """create exchange lan nsc"""
    account = args["account"]
    subaccount = args["subaccounts"][0]
    ret, nsc = config.network_service_configs_create(s, {
        "type": "exchange_lan",
        "network_service": ns_id,
        "role_assignments": [
            subaccount["implementation_role_assignment_id"],
            subaccount["noc_role_assignment_id"],
        ],
        "connection": subaccount["connection_id"],
        "billing_account": account["id"],
        "managing_account": account["id"],
        "consuming_account": subaccount["id"],
        "vlan_config": {
            "vlan_type": "dot1q",
            "vlan": 42,
            "vlan_ethertype": "0x8100",
        },
        "listed": False,
    })

    return ret, nsc


def get_required_network_features(s, args, ns_id):
    """Get required network features for network service"""
    return service.network_features_list(s, filters={
        "required": True,
        "network_service": ns_id,
    })


def create_rs_nfc(s, args, nf_id, nsc_id, ip):
    """Create network feature config"""
    account = args["account"]
    subaccount = args["subaccounts"][0]
    ret, nsc = config.network_feature_configs_create(s, {
        "type": "route_server",
        "billing_account": account["id"],
        "managing_account": account["id"],
        "consuming_account": subaccount["id"],
        "role_assignments": [
            subaccount["implementation_role_assignment_id"],
            subaccount["noc_role_assignment_id"],
        ],
        "as_set_v4": "AS-FOO",
        "network_feature": nf_id,
        "network_service_config": nsc_id,
        "asn": 9999999,
        "session_mode": "public",
        "bgp_session_type": "active",
        "ip": ip,
    })

    return ret, nsc
